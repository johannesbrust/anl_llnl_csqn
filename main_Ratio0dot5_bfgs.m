%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% main routine
% 
clc
clear
% clear all
% close all hidden

% dbmex state 

saveFiles = 1;

format long e
% warning off all
warning('off','backtrace');
warning('off','MATLAB:singularMatrix');
warning('off','MATLAB:nearlySingularMatrix');

addpath(genpath('./ALG')); 
addpath('/utrc/home/chiangn/nySoft/ASL/solvers/examples');

% add ampl interface
addpath(genpath('./INTERFACE'));

% path to nl files
 path = './TEST/NLFILE/Cuter_UnCon_Ori/';
 path_optSol  = './TEST/NLFILE/Cuter_UnCon_OptSol/';

% list = importdata([path,'list_cuter_run.txt']);
 list = importdata([path,'list_cuter_short.txt']);
%  list = importdata([path,'list_cuter_tiny.txt']);
 list = importdata([path_optSol,'list_cuter_withOptSol.txt']);

%  list = importdata([path_optSol,'list_cuter_short.txt']);

% get algorithmic options
par 	= get_options();

if strcmp(par.alg,'exact')
  par.exactRatio = 1;
elseif strcmp(par.alg,'bfgs')
  par.exactRatio = 0;
elseif strcmp(par.alg,'s-bfgs')
    if par.exactRatio   == 0 
	  par.alg='bfgs';
    elseif par.exactRatio == 1 
	  par.alg='exact';
    end
else 
  error('set Alg before calling');
end 

par.alg = 'bfgs';
par.sbfgsAlg = 2;
par.addUnknown = 'setRatio';
par.checkInertia = 0;
list = importdata([path,'list_cuter_short_bfgs.txt']);

% start clock
% tic

fprintf('Use Alg:   %s \n\n', par.alg);

% % add tests interface
% addpath(genpath('./TEST/NLFILE/Cuter_UnCon'));
% probRootname = 'morebv';
% name = ['TEST/NLFILE/Cuter_UnCon/' probRootname '.nl'];
% [prob,rawfunc] = read_ampl_prob(name);
% func = user_func_setRatio(rawfunc);
% [bool_conv,sol] = solvenlp(prob,par,func);

% sol

%  pause


%
if par.doLoop ==1 && ( strcmp(par.alg,'s-bfgs') && strcmp(par.addUnknown,'setRatio')  )
  ratioT_start = 0.1;
  ratioT_end   = 0.9;
elseif ( strcmp(par.alg,'s-bfgs') && strcmp(par.addUnknown,'setRatio')  )
  ratioT_start = par.exactRatio;
  ratioT_end   = par.exactRatio;  
else    
  ratioT_start = 1;
  ratioT_end   = 1;
end

for ratioT = ratioT_start:0.1:ratioT_end
par.exactRatio = ratioT;

idx = 0;
sumOK=0;
sumTotal=0;

% build projection matrix
if strcmp(par.addUnknown,'addNonLinObj_Proj')
    if(par.checkInertia < 1)
        error('must set checkInertia = 1 or 2 if addUnknown = addNonLinObj_Proj');
    end
    irow = zeros(par.num_unknown,1);
    jcol = zeros(par.num_unknown,1);
    kele = ones(par.num_unknown,1);
    for ii=1:par.num_unknown
        irow(ii) = ii;
        jcol(ii) = ii;
    end
end

for j = 1:length(list)
    aaa=1;
    clear spamfunc
    clearvars -except idx j list par path path_optSol ratioT ratioT_end ratioT_start sumOK sumTotal saveFiles sol irow jcol kele

    idx = idx + 1;
        fprintf('----> Running %s \n',list{j})   
    file = strcat(path,list{j});
    prob_O.name=file;
    
    if ~strcmp(par.addUnknown,'setRatio')
        optsolFile = strcat(path_optSol,list{j});
        [prob]=set_optSol_ampl(optsolFile,prob_O);
    else
	prob = prob_O;	
    end
    
    [ret,prob,par_U,rawfunc] = read_ampl_prob(file,prob,par);
    
    if ret==1
        % build projection matrix
        if strcmp(par.addUnknown,'addNonLinObj_Proj')
          prob.Pmat = sparse(irow,jcol,kele,par.num_unknown,prob.n,par.num_unknown);
        else
          prob.Pmat = eye(prob.n);
        end        
        
        % how to build the unknown part
        if strcmp(par.addUnknown,'addQuadObj')
            func = user_func_addQuadObj(rawfunc);
        elseif strcmp(par.addUnknown,'addNonLinObj')
            func = user_func_addNonLinObj(rawfunc);
        elseif strcmp(par.addUnknown,'setRatio')
            func = user_func_setRatio(rawfunc);
        elseif strcmp(par.addUnknown,'addNonLinObj_Proj')
             func = user_func_addNonLinObj_Projection(rawfunc);
        else
            error('set unknown part!');
        end
        

        
      [bool_conv,sol(idx)] = solvenlp(prob,par_U,func);
       sol(idx).name = strcat(list{j});
       fprintf('---Flag: %s, \t\tTotal Iter: %d, \tzoomLS: %5d \n',sol(idx).flag,sol(idx).nIter,sol(idx).zoomLS)
       if sol(idx).OK == 1
           sumOK = sumOK + sol(idx).OK;
       end
       sumTotal = sumTotal+1;
    else
        idx = idx-1;
    end
%     pause
end

end


fprintf('\n ---Solve: %d out of %d problems \n',sumOK,sumTotal)



if strcmp(par.alg,'exact')
    filename = "stats_hessian";
    if  par.checkInertia	== 1
        filename = strcat(filename, '_inertia');
    elseif  par.checkInertia	== 2
        filename = strcat(filename, '_descent');
    end
elseif strcmp(par.alg,'bfgs')
    filename = "stats_bfgs";
elseif strcmp(par.alg,'s-bfgs')
    filename = "stats_sbfgs";
    if strcmp(par.addUnknown,'addNonLinObj_Proj')
        filename = strcat(filename, '_low');
        if par.sbfgsAlg == 1 && par.checkInertia	== 1
            filename = strcat(filename, '_k_inertia');
        elseif par.sbfgsAlg == 1 && par.checkInertia	== 2
            filename = strcat(filename, '_k_descent');
        elseif par.sbfgsAlg == 2 && par.checkInertia	== 1
            filename = strcat(filename, '_kplus1_inertia');
        elseif par.sbfgsAlg == 2 && par.checkInertia	== 2
            filename = strcat(filename, '_kplus1_descent');
        end
    else
        if par.sbfgsAlg == 1
            filename = strcat(filename, '_k');
        elseif par.sbfgsAlg == 2 && par.checkInertia	== 1
            filename = strcat(filename, '_kplus1_inertia');
        elseif par.sbfgsAlg == 2 && par.checkInertia	== 2
            filename = strcat(filename, '_kplus1_descent');
        end
    end
end

if saveFiles == 1
    save(filename,'sol', 'par','sumOK','sumTotal'); 
    filename = strcat(filename,'.txt');
    fileID = fopen(filename,'w');
    fprintf(fileID,'name &flag &nIter &nFun \\\\ \n');
    for idx=1:sumTotal
        fprintf(fileID,'%s &%d &%d &%d \\\\ \n', sol(idx).name, sol(idx).OK,sol(idx).nIter,sol(idx).zoomLS);
    end
    fclose(fileID);
end



% getfield(sol(2),'x')

