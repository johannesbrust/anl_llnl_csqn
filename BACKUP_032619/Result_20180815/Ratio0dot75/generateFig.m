clc;
clear;

iterFig = 0;
nIterLimit = 1000;

if iterFig == 1
    fname1 = ['iter_subbfgs_' num2str(nIterLimit) 'maxiter'];
else
    fname1 = ['efun_subbfgs_' num2str(nIterLimit) 'maxiter'];
end

%%% table 3
% dataset = {'stats_hessian_inertia.mat' 'stats_hessian_descent.mat' 'stats_bfgs.mat' 'stats_sbfgs_k.mat' 'stats_sbfgs_kplus1_inertia.mat' 'stats_sbfgs_kplus1_descent.mat'};
dataset = {'stats_hessian_inertia.mat' 'stats_sbfgs_k.mat' 'stats_sbfgs_kplus1_inertia.mat' 'stats_bfgs.mat'};
logplot = 1;

num_set = size(dataset,2);

load(dataset{1});
[dummyInt, num_test] = size(sol);

n_iter = inf*ones(num_test,num_set);
n_eval = inf*ones(num_test,num_set);

names = cell(num_test);
max_num_test = num_test;

for id = 1:num_set
    load(dataset{id});
    [dummyInt, num_test] = size(sol)
    
    for i = 1:num_test
        
        if id == 1 && sol(i).OK>0 && sol(i).nIter <= nIterLimit
            n_iter(i,id) = sol(i).nIter;
            n_eval(i,id) = sol(i).zoomLS;
            names{i} = sol(i).name;
        elseif id ==1 && sol(i).nIter <= nIterLimit
            %             n_iter(i,id) = inf;
            %             n_eval(i,id) = inf;
            names{i} = sol(i).name;
        elseif sol(i).OK == 1 && sol(i).nIter <= nIterLimit
            for k=1:max_num_test
                findflag = strcmp(names{k},sol(i).name);
                if findflag ==1
                    n_iter(k,id) = sol(i).nIter;
                    n_eval(k,id) = sol(i).zoomLS;
                    break;
                end
            end
        end
    end
    
    
    %     perf(n_iter,logplot);
end

if iterFig ==1
    T = n_iter;
else
    T = n_eval;
end

figure
perf


axis([ 0.1 1.1*max_ratio 0 100 ]);
legend('Hes','SBFGS^M','SBFGS^P','BFGS','Location','southeast');

if iterFig ==1
    xlabel('log2 - Ratio of Iterations')
else
    xlabel('log2 - Ratio of Function Evaluations')
end

ylabel('% Problems')
set(gca, 'fontsize', 14)
box on;

print(fname1,'-depsc');


