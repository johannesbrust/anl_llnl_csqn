ModPath=./MODEL/Cuter_UnCon
NLPath=./NLFILE/Cuter_UnCon_OptSol

for file in $ModPath/*.mod
do
    cp ./ampl_script_template/genNL_noSuffix.inc genNL_noSuffix_temp.inc;
    
	filename=$(basename "$file")
    filename=${filename%.*}

	cp $ModPath/$filename.mod $filename\_temp.mod

	#solve prob or not
#	sed -i  "s/\bsolve;/#solve;/g" $filename\_temp.mod

	sed -i  "s/\bmodel temp/model $filename\_temp/g" genNL_noSuffix_temp.inc
	sed -i  "s/\bwrite btemp;/write b$filename;/g" genNL_noSuffix_temp.inc

	ampl genNL_noSuffix_temp.inc;	

	rm $filename\_temp.mod;
done

mv *.nl $NLPath
rm genNL_noSuffix_temp.inc
