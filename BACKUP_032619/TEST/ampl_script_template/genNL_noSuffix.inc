############################################################################
# Description: Running a single instance of OPF/Cartesian model with output
############################################################################

### Choose the solver to be used
option presolve 0;
#option auxfiles rc;
option solver ipopt;
#option show_stats 1;

### Declare the model file (Cartesian power flow)
model temp.mod; 

#solve;

write btemp;
