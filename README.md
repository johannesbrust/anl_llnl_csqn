# README #

Compact representations of structured quasi-Newton formulas.

Extension of "A Structured Quasi-Newton Algorithm for Optimizing with Incomplete Hessian Information", Petra, Chiang, Anitescu.