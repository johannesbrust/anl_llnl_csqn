% % function perf(T,logplot)



% if (nargin< 2)
%     logplot = 0;
% end

colors  = ['b' 'r' 'g' 'k' 'c' 'm' 'y' ];
lines   = ['-' '-' '-.' '--'];
markers = ['x' 'x' 'x' 'x' 'v' '^' 'o'];
[np,ns] = size(T);
minperf = min(T,[],2);
r = zeros(np,ns);
for p = 1: np
    if minperf(p) == 0
      minperf(p) = 1;
      r(p,:) = (T(p,:)+1)/(minperf(p));
    else
        r(p,:) = T(p,:)/minperf(p);
    end
end

sT = sort(T);
sR = r;
sTdm =  sort(r);

if (logplot)
    r = log2(r);
end
rr = sort(r);

max_ratio = max(max(r(r<inf)));
r(find(isnan(r))) = 3*max_ratio;
r(find(isinf(r))) = 3*max_ratio;
r = sort(r)
clf;
for s = 1: ns
    [xs,ys] = stairs(r(:,s),[1:np]/np);
    %     option = ['-' colors(s) markers(s) ];
    %     plot(xs,ys,option,'MarkerSize',3);
    
    option = ['-' colors(s) ];
    ys = ys*100;
    plot(xs,ys,option);
    hold on;
end


