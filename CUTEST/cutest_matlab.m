% ------------------ CUTEST MATLAB 16a MAC OSX 10.13 ---------------------%
%
% Script represents a template of how to invoke CUTEst 
% functions from within Matlab.
%
% Note: It is assumed that Matlab is started from within a terminal window.
% (Not started from the Mac Doc). In addition it is assumed that
% environment variables $CUTEST and $MASTSIF point to the 'cutest' and
% 'sif' folders of a CUTEst installation.
% ------------------------------------------------------------------------%
% Initial version: J.B., 02/27/19
% 
%

% Copy cutest library to current folder.
cmdcp ='cp $CUTEST/objects/mac64.osx.gfo/double/libcutest.a . ';
unix(cmdcp);

% Build CUTEst problem
probname    = 'BOX';
cmdcutest   = ['cutest2matlab_osx $MASTSIF/' probname];
unix(cmdcutest);

% Problem data
cu_prob     = cutest_setup();
cu_grad     = @cutest_grad;
cu_obj      = @cutest_obj;

x0          = cu_prob.x;

cutest_terminate();

